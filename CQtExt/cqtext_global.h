#ifndef CQTEXT_GLOBAL_H
#define CQTEXT_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(CQTEXT_LIBRARY)
#  define CQTEXTSHARED_EXPORT Q_DECL_EXPORT
#else
#  define CQTEXTSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // CQTEXT_GLOBAL_H
